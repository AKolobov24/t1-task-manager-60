package ru.t1.akolobov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.listener.AbstractListener;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Display available application commands.";

    @Override
    @NotNull
    public String getEventName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@commandListListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        context.getBeanProvider(AbstractListener.class, false)
                .forEach(listener -> System.out.println(listener.getEventName()));
    }

}
