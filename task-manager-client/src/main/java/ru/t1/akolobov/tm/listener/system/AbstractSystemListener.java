package ru.t1.akolobov.tm.listener.system;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.listener.AbstractListener;

@Getter
@Component
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

}
