package ru.t1.akolobov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataBackupLoadRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-backup";

    @NotNull
    public static final String DESCRIPTION = "Load backup data from base64 file.";

    @Override
    @NotNull
    public String getEventName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataBackupLoadListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        getDomainEndpoint().loadBackup(new DataBackupLoadRequest(getToken()));
    }

}
