package ru.t1.akolobov.tm.listener.user;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.akolobov.tm.api.endpoint.IUserEndpoint;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.exception.user.UserNotFoundException;
import ru.t1.akolobov.tm.listener.AbstractListener;

@Component
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return this.userEndpoint;
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return this.authEndpoint;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void displayUser(@Nullable final UserDto user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
