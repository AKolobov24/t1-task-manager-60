package ru.t1.akolobov.tm.listener.system;


import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.akolobov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @Override
    @NotNull
    public String getEventName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        ApplicationAboutResponse response = getSystemEndpoint().getAbout(new ApplicationAboutRequest());
        System.out.printf("Developer: %s\n", response.getName());
        System.out.printf("e-mail: %s\n", response.getEmail());
    }

}
