package ru.t1.akolobov.tm.component;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.event.ConsoleEvent;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@AllArgsConstructor
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    private void init() {
        es.scheduleWithFixedDelay(this::process, 0, 5, TimeUnit.SECONDS);
    }

    private void process() {
        File[] files = new File(propertyService.getCommandFolder()).listFiles();
        if (files == null || files.length == 0) return;
        for (File file : files) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            try {
                file.delete();
                publisher.publishEvent(new ConsoleEvent(fileName));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        init();
    }

}
