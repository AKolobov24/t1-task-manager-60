package ru.t1.akolobov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.ProjectDto;
import ru.t1.akolobov.tm.dto.request.ProjectListRequest;
import ru.t1.akolobov.tm.dto.response.ProjectListResponse;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Display list of all projects.";

    @Override
    @NotNull
    public String getEventName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectListListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();
        ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortType(Sort.toSort(sortType));
        ProjectListResponse response = getProjectEndpoint().list(request);
        @NotNull final List<ProjectDto> projectList = response.getProjectList();
        int index = 1;
        for (@NotNull final ProjectDto project : projectList) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
