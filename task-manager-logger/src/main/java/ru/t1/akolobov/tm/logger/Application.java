package ru.t1.akolobov.tm.logger;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.t1.akolobov.tm.logger.component.Bootstrap;
import ru.t1.akolobov.tm.logger.configuration.LoggerConfiguration;

public class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final AbstractApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        context.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
