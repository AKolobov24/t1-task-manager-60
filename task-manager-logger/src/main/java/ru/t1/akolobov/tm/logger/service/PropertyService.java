package ru.t1.akolobov.tm.logger.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.logger.api.IPropertyService;

@Getter
@Setter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    @Value("${server.enabled}")
    private String serverEnabled;

    @NotNull
    @Value("${consumer.url}")
    private String consumerUrl;

    @NotNull
    @Value("${consumer.queue}")
    private String consumerQueue;

    @NotNull
    @Value("${database.host}")
    private String databaseHost;

    @NotNull
    @Value("${database.port}")
    private String databasePort;

    @NotNull
    @Value("${database.name}")
    private String databaseName;

    @NotNull
    @Override
    public String getEmptyValue() {
        return EMPTY_VALUE;
    }

}
