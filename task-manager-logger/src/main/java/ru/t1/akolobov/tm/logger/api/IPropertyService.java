package ru.t1.akolobov.tm.logger.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {
    @NotNull String getServerEnabled();

    @NotNull String getConsumerUrl();

    @NotNull String getConsumerQueue();

    @NotNull String getDatabaseHost();

    @NotNull String getDatabasePort();

    @NotNull String getDatabaseName();

    @NotNull String getEmptyValue();

}
