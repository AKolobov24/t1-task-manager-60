package ru.t1.akolobov.tm.logger.listener;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.logger.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class EntityListener implements MessageListener {

    @NotNull
    @Autowired
    private LoggerService loggerService;

    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (message instanceof TextMessage) {
            @NotNull final TextMessage textMessage = (TextMessage) message;
            loggerService.log(textMessage.getText());
        }
    }

}
