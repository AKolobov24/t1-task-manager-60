package ru.t1.akolobov.tm.enumerated;

public enum FileFormat {

    JSON,
    XML,
    YAML

}
