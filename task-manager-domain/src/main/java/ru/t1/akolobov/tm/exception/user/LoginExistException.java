package ru.t1.akolobov.tm.exception.user;

public final class LoginExistException extends AbstractUserException {

    public LoginExistException() {
        super("Error! Login is already exists...");
    }

}
