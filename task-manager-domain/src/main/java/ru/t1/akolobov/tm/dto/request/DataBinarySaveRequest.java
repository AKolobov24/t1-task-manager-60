package ru.t1.akolobov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable String token) {
        super(token);
    }

}
