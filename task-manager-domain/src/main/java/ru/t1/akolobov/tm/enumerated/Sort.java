package ru.t1.akolobov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Sort {

    BY_NAME("Sort by name", "name"),
    BY_STATUS("Sort by status", "status"),
    BY_CREATED("Sort by created", "created");

    @NotNull
    private final String displayName;

    @NotNull
    private final String columnName;

    Sort(@NotNull String displayName, @NotNull String columnName) {
        this.displayName = displayName;
        this.columnName = columnName;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public String getColumnName() {
        return columnName;
    }

}
