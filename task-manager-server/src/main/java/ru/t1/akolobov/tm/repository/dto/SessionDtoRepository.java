package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.akolobov.tm.dto.model.SessionDto;
import ru.t1.akolobov.tm.enumerated.Sort;

import java.util.List;

@Repository
@Scope("prototype")
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM SessionDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public SessionDto findOneById(@NotNull String id) {
        return entityManager.find(SessionDto.class, id);
    }

    @NotNull
    @Override
    public List<SessionDto> findAll() {
        @NotNull String jpql = "SELECT s FROM SessionDto s";
        return entityManager.createQuery(jpql, SessionDto.class)
                .getResultList();
    }

    @Override
    public @NotNull List<SessionDto> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT s FROM SessionDto s ORDER BY s." + sort.getColumnName();
        return entityManager.createQuery(jpql, SessionDto.class)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(s) FROM SessionDto s";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        SessionDto sessionDTO = entityManager.find(SessionDto.class, id);
        if (sessionDTO == null) return;
        entityManager.remove(sessionDTO);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull String jpql = "DELETE FROM SessionDto s WHERE s.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll(@NotNull String userId) {
        @NotNull String jpql = "SELECT s FROM SessionDto s WHERE s.userId = :userId";
        return entityManager.createQuery(jpql, SessionDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionDto> findAll(@NotNull String userId, @NotNull Sort sort) {
        @NotNull String jpql = "SELECT s FROM SessionDto s WHERE s.userId = :userId " +
                "ORDER BY s." + sort.getColumnName();
        return entityManager.createQuery(jpql, SessionDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDto findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull String jpql = "SELECT s FROM SessionDto s " +
                "WHERE s.id = :id AND s.userId = :userId";
        return entityManager.createQuery(jpql, SessionDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public Long getSize(@NotNull String userId) {
        @NotNull String jpql = "SELECT COUNT(s) FROM SessionDto s WHERE s.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        SessionDto sessionDTO = findOneById(userId, id);
        if (sessionDTO == null) return;
        entityManager.remove(sessionDTO);
    }

}
