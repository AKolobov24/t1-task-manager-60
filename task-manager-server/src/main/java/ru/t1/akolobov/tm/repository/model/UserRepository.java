package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.model.IUserRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.User;

import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        @NotNull String jpql = "DELETE FROM Session s " +
                "WHERE s.user.id IN (SELECT u.id FROM User u)";
        entityManager.createQuery(jpql)
                .executeUpdate();
        jpql = "DELETE FROM Task t " +
                "WHERE t.user.id IN (SELECT u.id FROM User u)";
        entityManager.createQuery(jpql)
                .executeUpdate();
        jpql = "DELETE FROM Project p " +
                "WHERE p.user.id IN (SELECT u.id FROM User u)";
        entityManager.createQuery(jpql)
                .executeUpdate();
        jpql = "DELETE FROM User";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull String jpql = "SELECT u FROM User u";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public @NotNull List<User> findAll(@NotNull Sort sort) {
        @NotNull String jpql = "SELECT u FROM User u ORDER BY u." + sort.getColumnName();
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    public Long getSize() {
        @NotNull String jpql = "SELECT COUNT(u) FROM User u";
        return entityManager.createQuery(jpql, Long.class)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull String id) {
        User user = entityManager.find(User.class, id);
        if (user == null) return;
        entityManager.remove(user);
    }


    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull String jpql = "SELECT u FROM User u WHERE u.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull String jpql = "SELECT u FROM User u WHERE u.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
