package ru.t1.akolobov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.akolobov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.akolobov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.akolobov.tm.dto.model.TaskDto;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private final ITaskDtoRepository taskRepository;

    @NotNull
    @Autowired
    private final IProjectDtoRepository projectRepository;

    @Override
    @Transactional
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.forEach(t -> taskRepository.removeById(userId, t.getId()));
        projectRepository.removeById(userId, projectId);
    }

    @Override
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDto task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskRepository.update(task);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(final @Nullable String userId, final @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final TaskDto task = Optional.ofNullable(taskRepository.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskRepository.update(task);
    }

}
