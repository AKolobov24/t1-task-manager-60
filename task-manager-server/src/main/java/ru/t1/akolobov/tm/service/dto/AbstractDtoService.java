package ru.t1.akolobov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.api.repository.dto.IDtoRepository;
import ru.t1.akolobov.tm.api.service.dto.IDtoService;
import ru.t1.akolobov.tm.dto.model.AbstractDtoModel;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractDtoModel> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected IDtoRepository<M> repository;

    @Override
    @Transactional
    public void add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.update(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> newModels;
        newModels = new ArrayList<>(repository.add(models));
        return newModels;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        List<M> newModels;
        newModels = new ArrayList<>(repository.set(models));
        return newModels;
    }

    @Override
    @Transactional
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public Long getSize() {
        return repository.getSize();
    }

    @Override
    @Transactional
    public void remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.removeById(id);
    }

}
