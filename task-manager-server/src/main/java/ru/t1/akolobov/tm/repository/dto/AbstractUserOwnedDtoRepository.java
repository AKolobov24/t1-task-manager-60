package ru.t1.akolobov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.akolobov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel>
        extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        add(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        update(model);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        removeById(userId, model.getId());
    }

    private void removeAll(final @NotNull List<M> models) {
        if (models.isEmpty()) return;
        models.forEach(this::remove);
    }

}
