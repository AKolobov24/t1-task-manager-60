package ru.t1.akolobov.tm.data.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static Task createTask() {
        return new Task("task-1", "task-1-desc");
    }

    @NotNull
    public static Task createTask(@NotNull final User user) {
        @NotNull Task task = createTask();
        task.setUser(user);
        return task;
    }

    @NotNull
    public static List<Task> createTaskList(@NotNull final User user) {
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull Task task = new Task("task-" + i, "task-" + i + "-desc");
            task.setUser(user);
            taskList.add(task);
        }
        return taskList;
    }

}
