package ru.t1.akolobov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.akolobov.tm.configuration.ServerConfiguration;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestSession.createSession;
import static ru.t1.akolobov.tm.data.model.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final static ApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    private static EntityManager entityManager;

    @BeforeClass
    public static void prepareConnection() {
        entityManager = context.getBean(EntityManager.class);
        @NotNull final UserRepository userRepository = new UserRepository();
        entityManager.getTransaction().begin();
        userRepository.add(USER1);
        userRepository.add(USER2);
        entityManager.getTransaction().commit();
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull final UserRepository userRepository = new UserRepository();
        entityManager.getTransaction().begin();
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Before
    public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    @After
    public void clearData() {
        @NotNull final SessionRepository repository = new SessionRepository();
        entityManager.getTransaction().begin();
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
        entityManager.getTransaction().commit();
    }

    @Test
    public void add() {
        @NotNull final SessionRepository repository = new SessionRepository();
        Assert.assertTrue(repository.findAll().isEmpty());
        @NotNull final Session session = createSession(USER1);
        repository.add(session);
        entityManager.getTransaction().commit();
        Assert.assertEquals(session, repository.findAll().get(0));
        Assert.assertEquals(1, repository.findAll().size());
    }

    @Test
    public void clear() {
        @NotNull SessionRepository repository = new SessionRepository();
        @NotNull final List<Session> sessionList = createSessionList(USER1);
        sessionList.forEach(repository::add);
        @NotNull final Session user2Session = createSession(USER2);
        repository.add(user2Session);
        entityManager.getTransaction().commit();
        Assert.assertEquals(sessionList.size() + 1, repository.findAll().size());
        entityManager.getTransaction().begin();
        repository.clear(USER1_ID);
        entityManager.getTransaction().commit();
        Assert.assertEquals(1, repository.findAll().size());
        Assert.assertEquals(user2Session, repository.findAll().get(0));
    }

    @Test
    public void existById() {
        @NotNull final SessionRepository repository = new SessionRepository();
        @NotNull final Session session = createSession(USER1);
        repository.add(session);
        entityManager.getTransaction().commit();
        Assert.assertTrue(repository.existById(USER1_ID, session.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, session.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final SessionRepository repository = new SessionRepository();
        @NotNull final List<Session> user1SessionList = createSessionList(USER1);
        @NotNull final List<Session> user2SessionList = createSessionList(USER2);
        user1SessionList.forEach(repository::add);
        user2SessionList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(user1SessionList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2SessionList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull final SessionRepository repository = new SessionRepository();
        @NotNull final Session session = createSession(USER1);
        repository.add(session);
        entityManager.getTransaction().commit();
        @NotNull final String sessionId = session.getId();
        Assert.assertEquals(session, repository.findOneById(USER1_ID, sessionId));
        Assert.assertNull(repository.findOneById(USER2_ID, sessionId));
    }

    @Test
    public void getSize() {
        @NotNull final SessionRepository repository = new SessionRepository();
        @NotNull final List<Session> sessionList = createSessionList(USER1);
        sessionList.forEach(repository::add);
        entityManager.getTransaction().commit();
        Assert.assertEquals(sessionList.size(), repository.getSize(USER1_ID).intValue());
        entityManager.getTransaction().begin();
        repository.add(createSession(USER1));
        entityManager.getTransaction().commit();
        Assert.assertEquals((sessionList.size() + 1), repository.getSize(USER1_ID).intValue());
    }

    @Test
    public void remove() {
        @NotNull final SessionRepository repository = new SessionRepository();
        createSessionList(USER1).forEach(repository::add);
        @NotNull final Session session = createSession(USER1);
        repository.add(session);
        entityManager.getTransaction().commit();
        Assert.assertEquals(session, repository.findOneById(USER1_ID, session.getId()));
        entityManager.getTransaction().begin();
        repository.remove(session);
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

    @Test
    public void removeById() {
        @NotNull final SessionRepository repository = new SessionRepository();
        createSessionList(USER1).forEach(repository::add);
        @NotNull final Session session = createSession(USER1);
        repository.add(session);
        repository.removeById(USER1_ID, session.getId());
        entityManager.getTransaction().commit();
        Assert.assertNull(repository.findOneById(USER1_ID, session.getId()));
    }

}
